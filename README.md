---
title: README
author: Jan-Michael Rye
---

# Synopsis

[install_STEPS_in_venv.sh](install_STEPS_in_venv.sh) is a script to install [STEPS](https://github.com/CNS-OIST/STEPS) in a Python virtual environment.

# Dependencies

* [Bash](https://www.gnu.org/software/bash/bash.html) >= 5.0
* [CMake](https://www.cmake.org/)
* [Git](https://git-scm.com/)

In addition to the above, make sure to install all of the non-Python prerequisites specified in the STEPS readme. To install additional Python packages, add them to [requirements.txt](requirements.txt) before running the installation script.

# Usage

~~~
# Clone the repository before first use.
git clone 'https://gitlab.inria.fr/aistrosight/steps-venv.git'

# Inside the cloned directory.
./install_STEPS_in_venv.sh [<venv path>]
~~~

`<venv path>` is an optional path to the virtual environment. If not given, it will default to `venv` in the same directory as the script. The path to an existing virtual environment may be given. If the path does not exist then the virtual environment will be created.

The build directory persists after the command completes to enable quick installation into a new virtual environment. If you have no further need for the source code or built files, run the script [clean.sh](clean.sh) to remove them.
