#!/usr/bin/env bash
set -euo pipefail

GIT_URL=https://github.com/CNS-OIST/STEPS.git

SELF=$(readlink -f "${BASH_SOURCE[0]}")
SELF_DIR=${SELF%/*}

WORKING_DIR=$PWD
GIT_DIR=${GIT_URL##*/}
GIT_DIR=$WORKING_DIR/${GIT_DIR%.git}
BUILD_DIR=$WORKING_DIR/build
VENV_DIR=${1:-$WORKING_DIR/venv}
VENV_DIR=$(readlink -f "$VENV_DIR")

HLINE=--------------------------------------------------------------------------------
function message()
{
  echo "$HLINE"
  echo "$*"
  echo "$HLINE"
}

if [[ ! -e $GIT_DIR ]]
then
  message "Cloning $GIT_URL to $GIT_DIR..."
  git clone --recursive "$GIT_URL" "$GIT_DIR"
fi

message "Updating Python virtual environment..."
python3 -m venv "$VENV_DIR"
source "$VENV_DIR/bin/activate"
pip install -U pip
pip install -U -r "$SELF_DIR/requirements.txt"

PYTHON_SITE_PACKAGES=$(python -c 'from distutils.sysconfig import get_python_lib; print(get_python_lib())')
export PYTHON_SITE_PACKAGES
if [[ -z ${PYTHONPATH:-} ]]
then
  PYTHONPATH=$PYTHON_SITE_PACKAGES
else
  PYTHONPATH=$PYTHON_SITE_PACKAGES:$PYTHONPATH
fi
export PYTHONPATH

message "Building STEPS in $BUILD_DIR..."
mkdir -p "$BUILD_DIR"
cd -- "$BUILD_DIR"
rm -f CMakeCache.txt
cmake "$GIT_DIR"
cmake --build . -j "$(nproc)"

message "Installing STEPS to $VENV_DIR..."
DESTDIR="$VENV_DIR" cmake --install . --prefix "$VENV_DIR"

message "Testing STEPS Python package installation..."
python -c 'import steps;steps._greet();import steps.interface'

cat << MESSAGE
$HLINE
The STEPS Python package appears to be correctly installed.
To activate the virtual environment, run the following command:

    source ${VENV_DIR@Q}/bin/activate

To deactivate the virtual environment, run the command "deactivate".
$HLINE
MESSAGE
