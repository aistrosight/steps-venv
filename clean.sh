#!/usr/bin/env bash
set -euo pipefail

SELF=$(readlink -f "${BASH_SOURCE[0]}")
SELF_DIR=${SELF%/*}
cd -- "$SELF_DIR"
git clean -fdx
rm -vfr ./build ./STEPS
